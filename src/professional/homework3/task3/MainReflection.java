package professional.homework3.task3;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class MainReflection {
    public static void main(String[] args) {
        APrinter aPrinter = new APrinter();
        Class cls = aPrinter.getClass();

        try {
            Method method = cls.getMethod("print", int.class);
            method.invoke(aPrinter,100500);
        } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException e) {
            e.printStackTrace();
        }

    }
}