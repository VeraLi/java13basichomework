package professional.homework3.task2;

import professional.homework3.task1.IsLike;

public class Main {
    public static void main(String[] args) {
        writeDescription(Test.class);
    }
    public static void writeDescription(Class<?> clazz){
        if(!clazz.isAnnotationPresent(IsLike.class)){
            return;
        }
        IsLike isLike = clazz.getAnnotation(IsLike.class);
        System.out.println(isLike.value());
    }
}