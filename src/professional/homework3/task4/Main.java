package professional.homework3.task4;

import java.util.*;

public class Main {
    public static void main(String[] args) {
        Set<Class<?>> result = getAllInterfacesAndClasses(D.class);
        for (Class<?> a : result) {
            System.out.println(a.getName());
        }
    }

    public static Set<Class<?>> getAllInterfacesAndClasses(Class<?> cls){
        Set<Class<?>> interfaces1 = new HashSet<>();
        Class<?> anInterface = cls.getInterfaces()[0];
        while (anInterface.getInterfaces().length != 0){
            interfaces1.addAll(Arrays.asList(anInterface.getInterfaces()));
            anInterface = anInterface.getInterfaces()[0];
        }
        while (cls != Object.class ){
            interfaces1.addAll(Arrays.asList(cls.getInterfaces()));
            cls = cls.getSuperclass();

        }
        return interfaces1;
    }
}