package professional.homework3.task5;

public class Main {
    public static boolean check(String str) {
        int len1,len2;
        String tmp=str.replaceAll("[^()]","");
        do {
            len1 = tmp.length();
            tmp = tmp.replaceAll("\\(\\)", "");
            len2 = tmp.length();
        } while (len1 != len2);
        return (len2 == 0);
    }
    public static void main (String[] args) {

        System.out.println(check("(()()()) "));
        System.out.println(check(")( "));
        System.out.println(check("(() "));
        System.out.println(check("((())) "));
    }
}