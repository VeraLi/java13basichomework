package professional.homework3.task6;

import java.util.ArrayDeque;
import java.util.Deque;

public class Main {
    public static void main(String[] args) {
        String string1 = "{()[]()}";
        String string2 = "{)(}";
        String string3 = "[}";
        String string4 = "[{(){}}][()]{}";
        System.out.println("" + " - " + isCorrectBracketSequence(""));
        System.out.println(string1 + " - " + isCorrectBracketSequence(string1));
        System.out.println(string2 + " - " + isCorrectBracketSequence(string2));
        System.out.println(string3 + " - " + isCorrectBracketSequence(string3));
        System.out.println(string4 + " - " + isCorrectBracketSequence(string4));
    }

    public static boolean isCorrectBracketSequence(String sequence) {
        if (sequence.equals(""))
            return true;
        Deque<Character> stack = new ArrayDeque<>();
        char[] toCharSeq = sequence.toCharArray();
        for (int i = 0; i < sequence.length(); i++) {
            switch (toCharSeq[i]) {
                case '(' -> stack.push('(');
                case '{' -> stack.push('{');
                case '[' -> stack.push('[');
                case ')' -> {
                    if (stack.isEmpty() || stack.pop() != '(')
                        return false;
                }
                case '}' -> {
                    if (stack.isEmpty() || stack.pop() != '{')
                        return false;
                }
                case ']' -> {
                    if (stack.isEmpty() || stack.pop() != '[')
                        return false;
                }
            }
        }
        return stack.isEmpty();
    }
}