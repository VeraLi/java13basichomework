package professional.homework4.task7;

import java.util.Set;
import java.util.stream.Collectors;

public class Main {
    public static void main(String[] args) {
        System.out.println("cat cats - " + checkTwoStrings("cat", "cats"));
        System.out.println("cat cut - " + checkTwoStrings("cat", "cut"));
        System.out.println("cat nut - " + checkTwoStrings("cat", "nut"));
    }

    public static boolean checkTwoStrings(String string1, String string2) {
        if (Math.abs(string1.length() - string2.length()) > 1)
            return false;
        Set<Character> set1 = string1.chars().mapToObj(c -> (char) c).collect(Collectors.toSet());
        Set<Character> set2 = string2.chars().mapToObj(c -> (char) c).collect(Collectors.toSet());
        set1.removeAll(set2);
        return set1.size() < 2;
    }
}