package professional.homework4.task1;

import java.util.stream.IntStream;

public class Main {
    public static void main(String[] args) {
        System.out.println(sumEvenNum(1,100));
    }
    public static int sumEvenNum(int x, int y) {
        return IntStream
                .rangeClosed(x, y)
                .filter((p) -> p % 2 == 0)
                .sum();
    }
}