package professional.homework4.task4;

import java.util.Comparator;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        List<Double> list = List.of(15.8, 13.1, 3.5, 28.1, 26.1, 5.8, 13.4, 15.2);
        list.stream()
                .sorted(Comparator.reverseOrder())
                .forEach(System.out::println);
    }
}