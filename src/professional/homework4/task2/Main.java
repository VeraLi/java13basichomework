package professional.homework4.task2;

import java.util.List;
import java.util.Optional;

public class Main {
    public static void main(String[] args) {
        List<Integer> numbers = List.of(1, 2, 3, 4, 5);
        Optional<Integer> result =
                numbers.stream()
                        .reduce((sum,x) -> sum*x);
        System.out.println(result.get());

    }
}