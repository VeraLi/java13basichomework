package professional.homework4.task6;

import java.util.Set;
import java.util.stream.Collectors;

public class Main {
    public static void main(String[] args) {
        Set<Integer> set1 = Set.of(12, 70, 33);
        Set<Integer> set2 = Set.of(44, 52, 64);
        Set<Integer> set3 = Set.of(60, 78, 97);
        Set<Integer> set4 = Set.of(753, 438, 43);
        Set<Set<Integer>> set = Set.of(set1,set2,set3,set4);

        System.out.println(set.stream()
                .flatMap(Set::stream)
                .collect(Collectors.toSet()));
    }
}