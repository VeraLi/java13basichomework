package professional.homework4.task5;

import java.util.List;

public class Main {
    public static void main(String[] args) {
        List<String> list = List.of("abc", "def", "qqq");
        System.out.println( list.stream()
                .map(String::toUpperCase)
                .reduce((x,y)->x + ", " + y)
                .get());
    }
}