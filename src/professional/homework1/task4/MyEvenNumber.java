package professional.homework1.task4;

public class MyEvenNumber {
    private int n;

    public MyEvenNumber(int n) {
        if (n % 2 == 1)
            throw new IllegalArgumentException(n + " is a prime number");

        this.n = n;
    }
}