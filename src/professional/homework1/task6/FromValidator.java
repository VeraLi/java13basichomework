package professional.homework1.task6;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;

public class FromValidator {
    public static boolean checkName(String name) {
        try {
            if (name.matches("[A-Z|А-Я][a-z|а-я]{1,19}")) {
                return true;
            }
            throw new RuntimeException("RuntimeException: Имя должно быть с заглавной буквы и не более 20 символов");
        } catch (RuntimeException e) {
            System.out.println(e.getMessage());
        }
        return false;
    }

    public static boolean checkBirthdate(String birthdate) {
        LocalDate oldDate = LocalDate.of(1900, 1, 1);
        LocalDate today = LocalDate.now();
        try {
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy");
            LocalDate questionDate = LocalDate.parse(birthdate, formatter);
            if (questionDate.toEpochDay() >= oldDate.toEpochDay() && questionDate.toEpochDay() <= today.toEpochDay()) {
                return true;
            }
            throw new RuntimeException("Дата не может быть меньше 01.01.1900 и больше текущей");
        } catch (DateTimeParseException e) {
            System.out.println("DateTimeParseException: Не верный формат ввода");
        } catch (RuntimeException e) {
            System.out.println("RuntimeException: " + e.getMessage());
        }
        return false;
    }


    public static boolean checkGender(String gender) {
        try {
            Gender.valueOf(gender);
            return true;
        } catch (IllegalArgumentException e) {
            System.out.println("IllegalArgumentException: введите Male/Female ");
        }
        return false;
    }

    public static boolean checkHeight(String height) {

        try {
            double h = Double.parseDouble(height);
            if (h < 0)
                throw new RuntimeException("RuntimeException: Рост должен быть больше 0");
            else
                return true;
        } catch (NumberFormatException e) {
            System.out.println("NumberFormatException: рост введено не корректно");
        } catch (RuntimeException e) {
            System.out.println("RuntimeException: Рост не может быть меньше 0");
        }
        return false;
    }
}