package professional.homework1.task6;

public class Main {

    public static void main(String[] args) {
        String name = "Ivan";
        String birthdate = "03.05.2005";
        String gender = "Male";
        String height = "177.3";
        boolean result = true;

        if (FromValidator.checkName(name)) {
            System.out.println("Имя: " + name);
        } else result = false;
        if (FromValidator.checkBirthdate(birthdate)) {
            System.out.println("День рождения: " + birthdate);
        } else result = false;
        if (FromValidator.checkGender(gender)) {
            System.out.println("Пол: " + gender);
        } else result = false;
        if (FromValidator.checkHeight(height)) {
            System.out.println("Рост: " + height);
        } else result = false;

        if (result) {
            System.out.println("Все поля заполнены верно");
        } else {
            System.out.println("!!!!!Заполните все поля правильно!!!!!!");
        }
    }
}