select * from flowers;
select * from customers;
select * from orders;

select orders.id, customers.name, customers.phone, flower, amount
from customers
join orders on customers.id = orders.customer
where orders.id = 2;

select *
from orders
join customers ON orders.customer = customers.id
where (customer = 3) and (date > date - interval '1 month');

select id, flower, max(amount)
from orders
group by id
order by max(amount) desc
limit 1;

from flowers
join orders ON flowers.name = orders.flower;