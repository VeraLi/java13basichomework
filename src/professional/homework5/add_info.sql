insert into flowers (id, name, price) values (1, 'rose', 100);
insert into flowers (id, name, price) values (2, 'lily', 50);
insert into flowers (id, name, price) values (3, 'chamomile', 25);

insert into customers (id, name, phone) values (1, 'Alex', 9123456780);
insert into customers (id, name, phone) values (2, 'Max', 9214365879);
insert into customers (id, name, phone) values (3, 'Anna', 9224477088);
insert into customers (id, name, phone) values (4, 'Mark', 9346790125);
insert into customers (id, name, phone) values (5, 'Georg', 9037054422);

insert into orders (id, customer, flower, amount) values (1, 1, 'rose', 10);
insert into orders (id, customer, flower, amount) values (2, 2, 'rose', 20);
insert into orders (id, customer, flower, amount) values (3, 2, 'rose', 30);
insert into orders (id, customer, flower, amount) values (4, 3, 'lily', 40);
insert into orders (id, customer, flower, amount) values (5, 3, 'chamomile', 50);