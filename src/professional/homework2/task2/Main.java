package professional.homework2.task2;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        String s = "qwerty";
        String n = "qwetry";
        System.out.println(createArr(s).equals(createArr(n)));
    }

    public static List<Character> createArr(String s) {
        List<Character> list = new ArrayList<>();
        for (char value : s.toCharArray()) {
            list.add(value);
        }
        Collections.sort(list);
        return list;

    }
}