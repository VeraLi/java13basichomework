package professional.homework2.additional;

import java.util.*;

public class Main {
    public static void main(String... args) {

        String[] words = {"the", "day", "is", "sunny", "the", "the", "the",
                "sunny", "is", "is", "day"};

        int k = 4;

        System.out.println(Arrays.toString(result(words, k)));
    }


    public static String[] result(String[] words, int k) {

        Map<String, Integer> map = new TreeMap<>();
        for (String value : words) {

            int counter = 1;
            if (map.containsKey(value)) {
                counter = map.get(value);
                counter++;
            }
            map.put(value, counter);
        }
        List<Map.Entry<String, Integer>> list =
                new LinkedList(map.entrySet());
        Collections.sort(list, new Comparator<>() {

            @Override
            public int compare(Map.Entry<String, Integer> a,
                               Map.Entry<String, Integer> b) {
                return a.getValue() - b.getValue();
            }
        });
        Map<String, Integer> result = new LinkedHashMap<>();
        for (Map.Entry<String, Integer> entry : list) {

            result.put(entry.getKey(), entry.getValue());
        }
        List<String> listResult = new LinkedList<>(result.keySet());
        String[] wordsResult = new String[k];
        for (int i = 0; i < k; ) {
            for (int j = listResult.size() - 1; j >= 0; j--) {
                wordsResult[i] = listResult.get(j);
                i++;
                if (i == k) break;
            }
            break;
        }
        return wordsResult;
    }
}