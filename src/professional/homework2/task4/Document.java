package professional.homework2.task4;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Document {
    public int id;
    public String name;
    public int pageCount;

    public Document() {

    }

    public Document(int id, String name, int pageCount) {
        this.id = id;
        this.name = name;
        this.pageCount = pageCount;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public int getPageCount() {
        return pageCount;
    }

    public Map<Integer, Document> organizeDocuments(List<Document> documents) {
        Map<Integer, Document> orgDoc = new HashMap<>();
        for (int i = 0; i < documents.size(); i++) {
            orgDoc.put(documents.get(i).getId(), documents.get(i));
        }
        return orgDoc;
    }

    @Override
    public String toString() {
        return "Document{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", pageCount=" + pageCount +
                '}';
    }

    public static void main(String[] args) {
        List<Document> documents = new ArrayList<>();
        documents.add(new Document(1, "1 полугодие 2022", 3));
        documents.add(new Document(2, "2 полугодие 2022", 6));
        documents.add(new Document(3, "Годовой отчёт 2022", 12));

        Document document = new Document();
        Map<Integer, Document> orgDoc = document.organizeDocuments(documents);
        System.out.println("size: " + orgDoc.size());
        System.out.println("containsKey(0): " + orgDoc.containsKey(0));
        System.out.println("containsKey(3): " + orgDoc.containsKey(3));

        orgDoc.entrySet().forEach(
                e -> System.out.println("key: " + e.getValue().getId()
                        + ", name: " + e.getValue().getName()
                        + ", pageCount: " + e.getValue().getPageCount())
        );
    }
}