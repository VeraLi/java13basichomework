package professional.homework2.task1;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Main {
    public static void main(String[] args) {
        List<Integer> list = new ArrayList<>();
        list.add(1);
        list.add(2);
        list.add(3);
        list.add(1);
        list.add(4);
        System.out.println(arrayUniqueValue(list));

        List<String> list1 = new ArrayList<>();
        list1.add("Java");
        list1.add("Java");
        list1.add("C#");
        list1.add("C++");

        System.out.println(arrayUniqueValue(list1));

    }

    public static <T> Set<T> arrayUniqueValue(List<T> elements) {
        return new HashSet<>(elements);
    }
}