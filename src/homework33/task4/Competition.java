package homework33.task4;

import java.util.Arrays;
import java.util.Scanner;

public class Competition {

    public Competition() {
    }

    Participant participant = new Participant();
    Dog dog = new Dog();

    public void settingValues() {
        Scanner in = new Scanner(System.in);
        System.out.print("Введите количество участников: ");
        int n = in.nextInt();

        System.out.print("Введите имена участников: ");
        participant.setParticipant(new String[n]);
        for (int i = 0; i < participant.getParticipant().length; i++) {
            participant.getParticipant()[i] = in.next();
        }
        System.out.print("Введите клички питомцев: ");
        dog.setDogNickName(new String[n]);
        for (int i = 0; i < dog.getDogNickName().length; i++) {
            dog.getDogNickName()[i] = in.next();
        }
        System.out.print("Введите оценки: ");
        dog.setGrade(new int[n][3]);
        for (int i = 0; i < dog.getGrade().length; i++) {
            for (int j = 0; j < dog.getGrade()[i].length; j++) {
                dog.getGrade()[i][j] = in.nextInt();
            }
        }
    }
    public void averageMark() {
        double[] arr = new double[dog.getDogNickName().length];
        for (int i = 0; i < dog.getDogNickName().length; i++) {
            double sum = 0;
            for (int j = 0; j < dog.getGrade()[i].length; j++) {
                sum += dog.getGrade()[i][j];
            }
            dog.setResult(new double[]{(int) ((sum / dog.getGrade()[i].length) * 10) / 10.0});
            arr[i] = dog.getResult()[0];
            System.out.println("Участник: " + (participant.getParticipant()[i]) + ", Питомец: " + (dog.getDogNickName()[i])
                    + ", Оценки: " + Arrays.toString((dog.getGrade())[i]) +
                    ", Средний бал питомца: " + Arrays.toString(dog.getResult()));
        }
        winnersAndRunners(arr, participant.getParticipant(), dog.getDogNickName());
    }

    public void winnersAndRunners(double[] resultGrade, String[] participant, String[] dogNick) {
        for (int i = 0; i < resultGrade.length - 1; i++) {
            double number = resultGrade[i];
            int index = i;
            String name = participant[i];
            String dogs = dogNick[i];

            for (int j = i + 1; j < resultGrade.length; j++) {
                if (number < resultGrade[j]) {
                    number = resultGrade[j];
                    index = j;
                    name = participant[j];
                    dogs = dogNick[j];
                }
            }
            if (index != i) {
                resultGrade[index] = resultGrade[i];
                resultGrade[i] = number;
                participant[index] = participant[i];
                participant[i] = name;
                dogNick[index] = dogNick[i];
                dogNick[i] = dogs;
            }
        }
        for (int b = 0; b < 3; b++) {
            System.out.println(b + 1 + " место занял участник: " + (participant[b]) + ", Питомец: " + (dogNick[b])
                    + ", Средний бал питомца: " + resultGrade[b]);
        }
    }
}