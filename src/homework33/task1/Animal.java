package homework33.task1;

public abstract class Animal {
    final void Eat() {
        System.out.println("Ест");
    }

    final void Sleep() {
        System.out.println("Спит");
    }

    abstract public void WayOfBirth();

    abstract public void Move();
}
