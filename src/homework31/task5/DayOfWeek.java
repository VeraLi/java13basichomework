package homework31.task5;

public class DayOfWeek {
    private byte numberOfDay;
    private String day;

    public DayOfWeek(byte numberOfDay, String day) {
        this.numberOfDay = numberOfDay;
        this.day = day;
    }

    public String getDayOfWeek() {
        return numberOfDay + " " + day;
    }

}