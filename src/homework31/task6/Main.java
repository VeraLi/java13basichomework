package homework31.task6;

public class Main {

    public static void main(String[] args) {

        System.out.println(AmazingString.substringSearch(AmazingString.gatCharArr()));

        System.out.println(AmazingString.gatCharArr());
        System.out.println();

        char[] myCharArr = new char[]{'л', 'у', 'н', 'а'};
        AmazingString.setCharArr(myCharArr);
        System.out.println(AmazingString.gatCharArr());

        System.out.println();

        String myString = "светила небесные";
        AmazingString.setStringToCharArr(myString);
        System.out.println(AmazingString.gatCharArr());
        System.out.println();

        AmazingString.removingSpaces(AmazingString.gatCharArr());
        System.out.println(AmazingString.substringSearch(AmazingString.gatCharArr()));

        System.out.println(AmazingString.gatCharArr());
        System.out.println();

        AmazingString.unwrapLine(AmazingString.gatCharArr());
        System.out.println(AmazingString.gatCharArr());
    }
}