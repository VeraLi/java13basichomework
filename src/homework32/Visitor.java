package homework32;

import java.util.ArrayList;

public class Visitor {
    private String name;
    private Integer userID = null;
    private static final ArrayList<Integer> listOfUserIDs = new ArrayList<>();
    private Book book;

    public Visitor(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public Integer getUserID() {
        return userID;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void generateUserId() {
        Integer generatedID;

        do {
            generatedID = (int) (Math.random() * 1001);

        } while (listOfUserIDs.contains(generatedID));
        listOfUserIDs.add(generatedID);
        this.userID = generatedID;
    }

    public Book getBook() {
        return book;
    }

    public void setBook(Book book) {
        this.book = book;
    }
}