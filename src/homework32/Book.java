package homework32;

import java.util.ArrayList;
import java.util.List;

public class Book {

    private String bookTitle;
    private String author;
    private Visitor visitor;
    private int id;
    private List<Integer> visitorGrades;

    public List<Integer> getVisitorGrades() {
        return visitorGrades;
    }

    public Visitor getVisitor() {
        return visitor;
    }

    public Book(int id, String bookTitle, String author) {
        this.id = id;
        this.bookTitle = bookTitle;
        this.author = author;
    }

    public String getBookTitle() {
        return bookTitle;
    }

    public String getAuthor() {
        return author;
    }

    public void setBookTitle(String bookTitle) {
        this.bookTitle = bookTitle;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public int getId() {
        return id;
    }

    @Override
    public String toString() {
        return "Книга {" +
                "Book ID = '" + id + '\'' +
                ", название = '" + bookTitle + '\'' +
                ", автор = '" + author + '\'' +
                '}';
    }

    public void setVisitor(Visitor visitor) {
        this.visitor = visitor;
    }

    public void setEstimateVisitor(int grade) {
        if (visitorGrades == null) {

            setListGrades(new ArrayList<>());

            visitorGrades.add(grade);
        } else {
            visitorGrades.add(grade);
        }
    }

    public void setListGrades(List<Integer> visitorGrades) {
        this.visitorGrades = visitorGrades;
    }

    public double averageScore() {

        if (visitorGrades == null) {
            return 0;
        }

        double sum = 0;

        for (int i = 0; i < getVisitorGrades().size(); i++) {
            sum += getVisitorGrades().get(i);
        }


        return (Math.round(sum / getVisitorGrades().size() * 10)) / 10.0;
    }
}