package homework32;

public class Main {

    public static void main(String[] args) {
        Library library = new Library();

        library.showVisitorsList();

        library.addVisitor("Андрей");
        library.addVisitor("Сергей");
        library.addVisitor("Иван");
        library.addVisitor("Михаил");
        library.addVisitor("Александр");
        library.addVisitor("Максим");

        library.showVisitorsList();

        library.addVisitor("");

        library.showCollectionBooks();

        library.addBook("Руслан и Людмила", "А.С. Пушкин");
        library.addBook("Обломов", "И.А. Гончаров");
        library.addBook("Бедная Лиза", "Н.М. Карамзин");
        library.addBook("Медный всадник", "А.С. Пушкин");
        library.addBook("Гроза", "А.Н. Островский");
        library.addBook("Женитьба", "Н.В. Гоголь");
        library.addBook("Евгений Онегин", "А.С. Пушкин");

        library.assignBookToVisitor("Глеб", 2);

        library.assignBookToVisitor("Александр", -1);

// Закрепить книги за посетителями библиотеки
        library.assignBookToVisitor("Сергей", 3);
        library.assignBookToVisitor("Иван", 6);
        library.assignBookToVisitor("Михаил", 1);
        library.assignBookToVisitor("Александр", 5);

// Посмотреть User ID посетителей, которые взяли книгу
        library.showVisitorsList();

// Назначить книгу посетителю, которая уже занята
        library.assignBookToVisitor("Андрей", 3);

// Просмотреть коллекцию книг
        library.showCollectionBooks();

// Возвратить книгу, предварительно оценив её
        library.returnBook("Александр", 7);
        library.returnBook("Иван", 4);

// Ознакомиться с каталогом библиотеки
// после возврата книг
        library.showCollectionBooks();

        library.assignBookToVisitor("Андрей", 5);
        library.returnBook("Андрей", 4);
        library.assignBookToVisitor("Иван", 5);
        library.returnBook("Иван", 3);

        library.rateBook("Гроза");

        library.addBook("Руслан и Людмила", "А.С. Пушкин");

        library.deleteBook("Маленькая ведьма");

        library.deleteBook("Обломов");

        library.deleteBook("Бедная Лиза");

        System.out.println(library.getBookByName("Медный всадник"));

        System.out.println(library.getBookByName("Горе от ума"));

        System.out.println(library.getListOfBooksByAuthor("А.С. Пушкин"));

    }
}