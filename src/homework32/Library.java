package homework32;

import java.util.ArrayList;

public class Library {
    private final ArrayList<Book> listOfBooks = new ArrayList<>();

    private final ArrayList<Visitor> libraryVisitors = new ArrayList<>();

    private static int NUMBER_BOOKS;

    public void addBook(String name, String author) {
        if (name.isBlank() || author.isBlank()) {
            System.out.println("Пустое название или имя автора!");
            return;
        } else if (checkBookInLibrary(name)) {
            System.out.println();
            System.out.printf("Невозможно добавить книгу '%s'. Такая " +
                    "книга уже есть в библиотеке!\n", name);
            return;
        }
        this.listOfBooks.add(new Book(++NUMBER_BOOKS, name, author));

        System.out.printf("Книга '%s' успешно добавлена!\n", name);
    }

    private boolean checkBookInLibrary(String name) {
        for (Book book : listOfBooks) {
            if (name.equalsIgnoreCase(book.getBookTitle())) {
                return true;
            }
        }
        return false;
    }

    public void showCollectionBooks() {
        if (listOfBooks.size() == 0) {
            System.out.println();
            System.out.println("В библиотеку еще не были " +
                    "добавлены книги.");
            return;
        }

        System.out.println();
        System.out.println("\t\t\t=== ЭЛЕКТРОННЫЙ КАТАЛОГ БИБЛИОТЕКИ: ===");
        for (Book book : listOfBooks) {
            System.out.printf("Book ID: %s | Название: '%s' | Автор: %s | " +
                            "Кто-нибудь читает эту книгу? - %s\n",
                    book.getId(),
                    book.getBookTitle(), book.getAuthor(),
                    book.getVisitor() == null ? "Нет" : "Да, " + book.getVisitor().getName());

        }
    }

    public void deleteBook(String name) {

        if (!checkBookInLibrary(name)) {
            System.out.println();
            System.out.printf("Невозможно удалить книгу '%s'!" +
                    " Такой книги нет в библиотеке!\n", name);
            return;
        }

        for (Book book : listOfBooks) {
            if (book.getVisitor() != null && book.getBookTitle().equalsIgnoreCase(name)) {
                System.out.println("\nНевозможно удалить книгу '" + book.getBookTitle() +
                        "', поскольку ее читает " + book.getVisitor().getName() + ".");
                return;
            }
        }

        for (Book book : listOfBooks) {
            if (book.getBookTitle().equalsIgnoreCase(name)) {
                listOfBooks.remove(book);
                System.out.printf("\nКнига '%s' успешно удалена!\n", name);
                break;
            }
        }
    }

    public void addVisitor(String name) {
        if (name.isBlank()) {
            System.out.println("Имя посетителя не указано! " +
                    "Посетитель не добавлен.");
            return;
        }
        Visitor visitor = new Visitor(name);
        this.libraryVisitors.add(visitor);
        System.out.printf("Посетитель %s успешно добавлен!\n", name);
    }

    public void showVisitorsList() {
        if (libraryVisitors.size() == 0) {
            System.out.println("В библиотеке нет посетителей.");
            return;
        }

        System.out.println();
        System.out.println("\t\t === ПОСЕТИТЕЛИ: ===");
        for (Visitor visitor : libraryVisitors)
            System.out.printf("Имя: %s | User ID: %s\n",
                    visitor.getName(),
                    visitor.getUserID());
        System.out.println();
    }

    public Book getBookByName(String name) {
        for (Book book : listOfBooks) {
            if (book.getBookTitle().equalsIgnoreCase(name)) {
                System.out.println("\nПо запросу '" + name + "' была " +
                        "найдена следующая книга: ");
                return book;
            }
        }
        System.out.println();
        System.out.println("Такой книги нет в библиотеке!");
        return null;
    }

    public ArrayList<Book> getListOfBooksByAuthor(String author) {
        ArrayList<Book> resultArray = new ArrayList<>();
        System.out.println("\nПо запросу '" + author + "' были" +
                " найдены следующие книги: ");
        for (Book book : listOfBooks) {
            if (book.getAuthor().equalsIgnoreCase(author))
                resultArray.add(book);
        }
        return resultArray;
    }

    public void assignBookToVisitor(String name, int id) {
        if (libraryVisitors.size() == 0) {
            System.out.println("В библиотеке еще не было посетителей.");
            return;
        }

        for (Visitor visitor : libraryVisitors) {
            if (name.equalsIgnoreCase(visitor.getName())) {

                if (visitor.getUserID() == null) {
                    visitor.generateUserId();
                }

                if (id <= 0 || id > listOfBooks.size()) {
                    System.out.println("\nКнига с идентификатором '" + id +
                            "' отсутствует в библиотеке!");
                    return;
                }

                for (Book book : listOfBooks) {

                    if (visitor.getBook() != null && id == book.getId()) {
                        System.out.println("Нельзя одолжить книгу! " +
                                "У посетителя есть книга, которую необходимо " +
                                "вернуть.");
                        return;
                    }

                    if (book.getVisitor() != null && id == book.getId()) {
                        System.out.println(visitor.getName() + ", книга занята! Пожалуйста, выбери " +
                                "другую.");
                        return;
                    }

                    if (id == book.getId()) {
                        book.setVisitor(visitor);
                        visitor.setBook(book);
                        return;
                    }
                }
            }
        }
        System.out.printf(""" 

                Посетитель %s не найден! Невозможно закрепить за ним книгу. 
                """, name);
    }

    public void returnBook(String visitorName, int mark) {
        if (libraryVisitors.size() == 0) {
            System.out.println("В библиотеке еще не было посетителей!");
            return;
        }

        for (Visitor visitor : libraryVisitors) {
            if (visitorName.equals(visitor.getName())) {
                if (mark < 1 || mark > 7) {
                    System.out.println("Неверный формат! Оценка должна быть от 1 до 7.");
                } else {
                    visitor.getBook().setEstimateVisitor(mark);

                    visitor.getBook().setVisitor(null);
                    visitor.setBook(null);
                }
                return;
            }
        }
        System.out.println("Посетитель " + visitorName + " не найден!");
    }

    public void rateBook(String name) {
        if (listOfBooks.size() == 0) {
            System.out.println("В библиотеку еще не были " +
                    "добавлены книги.");
            return;
        }

        System.out.println("\nНазвание книги | Оценка");
        for (Book book : listOfBooks) {
            if (name.equals(book.getBookTitle())) {
                System.out.println("'" + book.getBookTitle() + "'" +
                        "| " + book.averageScore());
            }
        }
    }

}